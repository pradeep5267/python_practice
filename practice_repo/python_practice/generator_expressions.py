#%%
#genrator
it = ['apple',5,0.7]
vals = ((print(type(x))) for x in it)
for x in range(len(it)):
    next(vals)
#%%
#functions
def divide(a,b):
    try:
        return (True,a/b)
    except :
        return (False,None)

result, value = divide(0,0)

print(value)


#%%
#namedtuple
import collections
Fruit = collections.namedtuple('Fruit',['apple','number','pear'])
g = Fruit('pine',3,'string')
print(g)
print(type(g))
print(g.number)


#%%
#Raising errors
def divide(a,b):
    try:
        return (True,a/b)
    except ZeroDivisionError as e:
        raise ValueError('0 as input') from e

result, value = divide(0,0)
print(value)

#%%
#Generators and yield
address = 'Four score and seven years ago..'
not_words = 5

def words(text):
    if text:
        yield 0
    else:
        yield 1
    for index, letter in enumerate(text):
        if letter == ' ':
            yield index+1
gen = words(not_words)

#%%
#iter container
num = [1,9,2,8]
class read_nums(object):
    def __init__(self,num):
        self.num = num
    def __iter__(self):
        for x in num:
            yield x
y = read_nums(num)
print(sum(y))

#%%
# Default none option for multiple initialization
import datetime as datetime
def log(message, when=None):

    '''Log a message with a timestamp.
    Args:
    message: Message to print.
    when: datetime of when the message occurred.
    Defaults to the present time.
    '''
    when = datetime.datetime.now() if when is None else when
    print('%s: %s' % (when, message))
log('abc')
log('xyz')

#%%
#inheritence
class Grades():
    def __init__(self,grades):
        self._grades = grades
        print(grades)

class SimpleGradebook(Grades):
        def __init__(self):
            print(_grades)
            #self._grades = {}
        def add_student(self, name):
            self._grades[name] = []
        def report_grade(self, name, score):
            self._grades[name].append(score)
        def average_grade(self, name):
            grades = self._grades[name]
            return sum(grades) / len(grades)

grade = {'grade':7}

book = SimpleGradebook(grade)
book.add_student('abc')
book.report_grade('abc',50)

#%%
#inheritance simple
class Parent():
    def __init__(self,name,place):
        self._name = name
        self._place = place
    def printfunc(self):
        print(self._name)
        print(self._place)

class Child(Parent):
    def add_values(self,animal):
        self._animial = animal
p = Parent('abc','Mumbai')
p.printfunc()
c = Child('xyz','New Mumbai')
c.add_values('dog')
c.printfunc()

#%%
#Wrapper function
from functools import wraps
def trace(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        print('%s(%r, %r) -> %r' %
        (func.__name__, args, kwargs, result))
        return result
    return wrapper


@trace
def fibonacci(n):
    '''Return the n-th Fibonacci number'''
    if n in (0, 1):
            return n
    return (fibonacci(n - 2) + fibonacci(n - 1))
fibonacci(5)

#%%
#copyreg
'''takes in 2 parameters, the constructor to be pickled and copyreg
type pickle format'''