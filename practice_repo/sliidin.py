
# coding: utf-8

# In[15]:


from skimage.feature import peak_local_max

import numpy as np
import cv2
import os
import matplotlib.pyplot as plt

import skimage
from scipy import ndimage as ndi

from skimage import feature
from skimage import data
from skimage.feature import match_template

import imutils


data_path = 'D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\480x360'
jpg_names = []
json_list = []
jpg_json_list = []

for x in os.listdir(data_path):
    if '.jpg' in x:
        jpg_names.append(x)
#print(jpg_names)

for x in os.listdir(data_path):
    if '.json' in x:
        json_list.append(x)
#print(json_list)


# In[16]:


# Load an color image in grayscale
img = cv2.imread(os.path.join(data_path,jpg_names[7]),3)
#img = img [...,::-1]
plt.imshow(img)
#7-->found,
#9-->nothing

og_img = img
#plt.imshow(og_img)

b,g,r = cv2.split(img)

blur = cv2.GaussianBlur(img,(7,7),0)
#plt.imshow(blur)


#cv2.imshow('guassian blur',blur)
#cv2.imshow('og img',img)
#cv2.waitKey(0) 

kernel = np.ones((7,7), np.uint8)
img_erosion = cv2.erode(blur, kernel, iterations=1) 
img_dilation = cv2.dilate(blur, kernel, iterations=1) 

#plt.imshow(img_erosion)

  
#cv2.imshow('Input', img) 
#cv2.imshow('Erosion', img_erosion) 
#cv2.imshow('Dilation', img_dilation) 
  
#cv2.waitKey(0) 

#plt.imshow(img_dilation)

mask_maker = img_erosion

edges = cv2.Canny(mask_maker, 50,70) 
kernel = np.ones((5,5), np.uint8)
#img_edges1 = cv2.erode(edges, kernel, iterations=2)
img_edges2 = cv2.dilate(edges, kernel, iterations=1)

#plt.imshow(img_edges1)
#cv2.imshow('edges',edges)
#cv2.waitKey(0)
#plt.imshow(img_edges2)



blank_image0 = np.zeros((50,250,3), np.uint8)
blank_image1 = np.zeros((50,150,3), np.uint8)
blank_image2 = np.zeros((250,50,3), np.uint8)
blank_image3 = np.zeros((150,50,3), np.uint8)
blank_image4 = np.zeros((50,50,3), np.uint8)
blank_image5 = np.zeros((480,360,3), np.uint8)
#plt.imshow(blank_image)
#template_250x50 = blank_image

cv2.imwrite('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates\\1_50,250.jpg',blank_image0)
cv2.imwrite('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates\\2_50,150.jpg',blank_image1) 
cv2.imwrite('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates\\3_250,50.jpg',blank_image2) 
cv2.imwrite('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates\\4_150,50.jpg',blank_image3) 
cv2.imwrite('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates\\5_50,50.jpg',blank_image4) 
cv2.imwrite('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates\\6_480,360.jpg',blank_image5) 

point=[]
pt1=[]
pt2=[]
large_image = img_edges2
large_image_copy = img_edges2
height = large_image.shape[0]
height = large_image.shape[1]
method = cv2.TM_SQDIFF
for y in os.listdir('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates'):
    template = cv2.imread(os.path.join('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates',y),0)
    for x in range(1):
        for i in range(0,height):
            for j in range(0,width):           
                pt1=(0+i,0+j)
                pt2=(20+i,20+j)
                point.append([pt1,pt2])
                cv2.rectangle(res,pt1,pt2,(255,0,0))large_image = img_edges2
large_image_copy = img_edges2
height = large_image.shape[0]
height = large_image.shape[1]
stepSize = 50
for y in os.listdir('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates'):
    template = cv2.imread(os.path.join('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates',y),0)
# In[18]:


import cv2
import matplotlib.pyplot as plt
import numpy as np
box_list = []
window = np.empty
ht=0
wt=0
box_dict = {}
x1_list = []
y1_list = []
x2_list = []
y2_list = []
x=0
y=0

# read the image and define the stepSize and window size 
# (width,height)
#im_gray = cv2.imread('grayscale_image.png', cv2.IMREAD_GRAYSCALE)
template = cv2.imread('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates\\3_250,50.jpg',cv2.IMREAD_GRAYSCALE)

thresh = 127
template = cv2.threshold(template, thresh, 255, cv2.THRESH_BINARY)[1]


image = img_edges2 # your image path
tmp = image # for drawing a rectangle
dimensions = tmp.shape

ht = tmp.shape[0]
wt = tmp.shape[1]

stepSize = int(template.shape[0])
print(stepSize)

(w_width, w_height) = (template.shape[1], template.shape[0]) # window size

for y in range(0, image.shape[0],2):
    for x in range(0, image.shape[1],2):
        window = image[y:y + w_height,x:x + w_width]
        #cv2.imshow('window',window)
        #cv2.waitKey(0)
        if np.array_equal(window,template)== True:
            cv2.rectangle(tmp, (x, y), (x + w_width, y + w_height), (255, 255, 255), 2) # draw rectangle on image
            box_list.append(zip((x, y),(x + w_width, y + w_height)))
            print('match found!!!!')
            
            x1_list.append(x)
            x2_list.append(x + w_width)
            y1_list.append(y)            
            y2_list.append(y + w_height)
        else:
            pass
            #print('no match for 50x250')
            #plt.show()

            # classify content of the window with your classifier and  
            # determine if the window includes an object (cell) or not

      # draw window on image
      #cv2.rectangle(tmp, (x, y), (x + w_width, y + w_height), (255, 0, 0), 2) # draw rectangle on image
      #plt.imshow(np.array(tmp).astype('uint8'))
# show all windows
#plt.show()

imS = cv2.resize(tmp, (960, 540))                    # Resize image
cv2.imshow("output", imS)                            # Show image
cv2.waitKey(0)
print()
print()
print(box_list)
box_dict['x1'] = x1_list
box_dict['x2'] = x2_list
box_dict['y1'] = y1_list
box_dict['y2'] = y2_list
print()
print()
print(box_dict)


# In[ ]:


x1=x1_list[0]
x_list=[]
count=1
for x in range(1,len(x1_list)+1):
    if (x1_list[count]%(x1*count)==0):
        x_list.append(x1_list[count])
print        


# In[11]:


print(x1_list)


# In[16]:


import pandas as pd
data = [x1_list,x2_list,y1_list,y2_list]
cols = ['x1','x2','y1','y2']
pd.DataFrame(data )
    


# In[14]:


x_list = []
for x in range(len(x1_list)):
    print(x1_list[x+1])
    x2=x2_list[x]
    if (x1-x2)<=52:
        x_list.append(x1_list[x])
#print(len(x1_list))
#print(len(x_list))


# In[20]:


print(x_list)


# In[13]:


print(cont_list)


# In[13]:


import more_itertools as mit
iterable = box_dict['x1']
for group in mit.consecutive_groups(iterable):
    group = list(group)
    if len(group) == 1:
        print(group[0])
    else:
         print(group[0], group[-1])
#[list(group) for group in mit.consecutive_groups(iterable)]


# In[15]:


from operator import itemgetter
data = box_dict['x1']
for k, g in groupby(enumerate(data), lambda (i,x):i-x):
    print map(itemgetter(1), g)

import cv2
import matplotlib.pyplot as plt
import numpy as np
window = np.empty

# read the image and define the stepSize and window size 
# (width,height)
#im_gray = cv2.imread('grayscale_image.png', cv2.IMREAD_GRAYSCALE)
template = cv2.imread('D:\\ml_dl_saves\\Hello_sivi_task2\\dataset\\480x360\\templates\\1_50,250.jpg',cv2.IMREAD_GRAYSCALE)
thresh = 127
template = cv2.threshold(template, thresh, 255, cv2.THRESH_BINARY)[1]

image = img_edges2 # your image path
tmp = image # for drawing a rectangle
stepSize = 100
(w_width, w_height) = (template.shape[1], template.shape[0]) # window size

for x in range(0, image.shape[1] - w_width , stepSize):
    for y in range(0, image.shape[0] - w_height, stepSize):
        window = image[x:x + w_width, y:y + w_height]
        #cv2.imshow('window',window)
        #cv2.waitKey(0)
        if np.array_equal(window,template)== True:
            cv2.rectangle(tmp, (x, y), (x + w_width, y + w_height), (255, 255, 255), 2) # draw rectangle on image
            print('match found!!!!')
            cv2.imshow('tmp',tmp)
            cv2.waitKey(0)
        else:                
            print('no match for 50x250')
            #plt.show()

            # classify content of the window with your classifier and  
            # determine if the window includes an object (cell) or not

      # draw window on image
      #cv2.rectangle(tmp, (x, y), (x + w_width, y + w_height), (255, 0, 0), 2) # draw rectangle on image
      #plt.imshow(np.array(tmp).astype('uint8'))
# show all windows
#plt.show()

# In[39]:


for x,y in box_list:
    print(x)
    print(y)
    print(' ')
cv2.rectangle(tmp, (0, 584), (x + w_width, y + w_height), (255, 255, 255), 2) # draw rectangle on image

